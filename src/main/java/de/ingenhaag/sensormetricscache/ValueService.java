package de.ingenhaag.sensormetricscache;

import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.util.HashMap;
import java.util.List;
import java.util.Objects;

@Component
public class ValueService {

  private static final String METRIC_NAME = "luftdaten.value";

  @Autowired
  MeterRegistry meterRegistry;

  private final HashMap<String, Double> doubleCache = new HashMap<>();
  private final HashMap<String, Integer> integerCache = new HashMap<>();

  public Mono<Void> setData(ValueWrapper data) {
    meterRegistry.gauge("luftdaten.software.version", List.of(Tag.of("version", data.getSoftwareVersion())), 1);
    data.getValues().stream()
        .filter(Objects::nonNull)
        .filter(valueModel -> valueModel.getValue() != null && valueModel.getValueType() != null)
        .forEach(valueModel -> {
          if (valueModel.getValue().contains(".")) {
              doubleCache.put(valueModel.getValueType(), Double.valueOf(valueModel.getValue()));
              meterRegistry.gauge(METRIC_NAME, List.of(Tag.of("value_type", valueModel.getValueType())), doubleCache, value -> value.get(valueModel.getValueType()));
          } else {
              integerCache.put(valueModel.getValueType(), Integer.valueOf(valueModel.getValue()));
              meterRegistry.gauge(METRIC_NAME, List.of(Tag.of("value_type", valueModel.getValueType())), integerCache, value -> value.get(valueModel.getValueType()));
          }
        });
    return Mono.empty();
  }

}
