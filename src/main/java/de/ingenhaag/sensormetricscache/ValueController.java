package de.ingenhaag.sensormetricscache;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/api/sensorvalues")
public class ValueController {

  private static final Logger LOGGER = LoggerFactory.getLogger(ValueController.class);

  @Autowired
  ValueService service;

  @PostMapping("/data")
  public Mono<Void> updateData(@RequestBody ValueWrapper data) {
    LOGGER.debug("received data {}", data);
    service.setData(data);
    return Mono.empty();
  }
}
