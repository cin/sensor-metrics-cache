package de.ingenhaag.sensormetricscache;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class ValueWrapper {

  @JsonProperty("esp8266id")
  private String id;
  @JsonProperty("software_version")
  private String softwareVersion;

  @JsonProperty("sensordatavalues")
  private List<ValueModel> values;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getSoftwareVersion() {
    return softwareVersion;
  }

  public void setSoftwareVersion(String softwareVersion) {
    this.softwareVersion = softwareVersion;
  }

  public List<ValueModel> getValues() {
    return values;
  }

  public void setValues(List<ValueModel> values) {
    this.values = values;
  }

  @Override
  public String toString() {
    return "ValueWrapper{" +
        "id='" + id + '\'' +
        ", softwareVersion='" + softwareVersion + '\'' +
        ", values=" + values +
        '}';
  }
}
