package de.ingenhaag.sensormetricscache;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ValueModel {

  @JsonProperty("value_type")
  private String valueType;

  @JsonProperty("value")
  private String value;

  public String getValueType() {
    return valueType;
  }

  public void setValueType(String valueType) {
    this.valueType = valueType;
  }

  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }

  @Override
  public String toString() {
    return "ValueModel{" +
        "valueType='" + valueType + '\'' +
        ", value='" + value + '\'' +
        '}';
  }
}
